/*.
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import modelo.*;
import Vista.jifProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author danie
 */
public class Controlador implements ActionListener{
    private dbProductos db = new dbProductos();
    private jifProductos vista;
    private boolean esActualizar;
    private int idProductos;
    
    //Constructor
    public Controlador(jifProductos vista, dbProductos db) {
        this.vista = vista;
        this.db = db;   
        
        //Activacion de botones
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    //Metodos helpers
    public void iniciarVista() {
        vista.setTitle(":: Productos ::");
        vista.setVisible(true);
        vista.resize(850, 850);
        
        try {
            this.ActualizarTabla(db.lista());
        }catch(Exception e){
            JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
        }
    }
    
    public String convertirAñoMesDia(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    
    public void convertirStringDate(String fecha){
        try { 
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.jdcFecha.setDate(date);
        }catch (ParseException e) {
        System.err.print(e.getMessage());
        }
    }
    
    public void limpiar(){
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jdcFecha.setDate(new Date());
    }
    
    public void cerrar(){
        int res = JOptionPane.showConfirmDialog(vista, "Desea salir del Sistema?", "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if(res == JOptionPane.YES_NO_OPTION){
            vista.dispose();
        }
    }
    
    public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
    }
    
    public void deshabilitar(){
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
    }
    
    public boolean validar(){
        boolean exito = true;
        
        if(vista.txtCodigo.getText().equals("") ||
                vista.txtNombre.getText().equals("") ||
                vista.txtPrecio.getText().equals("")) exito = false;
        
        return exito;
    }
    
    public void ActualizarTabla(ArrayList<Productos>arr) {
        String campos[] = {"idProducto", "Codigo", "Nombre", "Precio", "Fecha"};
        
        String [][] datos = new String[arr.size()][15];
        int reglon = 0;
        for(Productos registro : arr) {
            datos[reglon][0] = String.valueOf(registro.getIdProductos());
            datos[reglon][1] = registro.getCodigo();
            datos[reglon][2] = registro.getNombre();

            datos[reglon][3] = String.valueOf(registro.getPrecio());
            datos[reglon][4] = registro.getFecha();

        reglon ++;
        }
        DefaultTableModel tb = new DefaultTableModel (datos, campos);
        vista.tblProductos.setModel(tb);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource() == vista.btnLimpiar) this.limpiar();
        if(ae.getSource() == vista.btnCancelar) { this.limpiar(); this.deshabilitar();}
        if(ae.getSource() == vista.btnCerrar) this.cerrar();
        if(ae.getSource() == vista.btnNuevo){this.limpiar(); this.habilitar(); this.esActualizar = false; vista.txtCodigo.requestFocus();}
        
        
        if(ae.getSource() == vista.btnBuscar) {
            Productos pro = new Productos();
            if(vista.txtCodigo.equals("")) {
                JOptionPane.showMessageDialog(vista, "Error al capturar el codigo");
                vista.txtCodigo.requestFocus();
            }
            else {
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    
                    if(pro.getIdProductos()!=0) {
                        //Aleluya si lo encontro
                        //Mostrar la informacion
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        this.convertirStringDate(pro.getFecha());
                        vista.btnBuscar.setEnabled(true);
                        vista.btnDeshabilitar.setEnabled(true); 
                        this.esActualizar = true;
                        this.idProductos = pro.getIdProductos();
                    } else JOptionPane.showMessageDialog(vista, "No se encontro el producto con codigo" + vista.txtCodigo.getText());
                }catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Error al buscar" + e.getMessage());
                }
            }
        }
        
        if (ae.getSource() == vista.btnDeshabilitar) {
            Productos pro = new Productos();
            pro.setIdProductos(this.idProductos);
            
            int res = JOptionPane.showConfirmDialog(vista, "Desea deshabiliar el producto", "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if(res == JOptionPane.YES_OPTION) {
                try {
                    db.deshabilitar(pro);
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());

                }catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al deshabilitar" + e.getMessage());
                }
            }
        }
        if(ae.getSource() == vista.btnGuardar) {
            Productos pro = new Productos();
            
        if(this.validar() == true) {
        //Todo bien
        pro.setCodigo(vista.txtCodigo.getText());
        pro.setNombre(vista.txtNombre.getText());
        pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
        pro.setFecha(this.convertirAñoMesDia(vista.jdcFecha.getDate()));
        try {
        if(this.esActualizar == false) {
            db.insertar(pro);
            JOptionPane.showMessageDialog(vista, "Se agrego con exito el Producto con codigo" + vista.txtCodigo.getText());
            this.limpiar();
            this.deshabilitar();
            this.ActualizarTabla(db.lista());
        } else {
            //Se actualizo
            pro.setIdProductos(idProductos);
            db.actualizar(pro);
            JOptionPane.showMessageDialog(vista, "Se actualizo con exito el Producto con codigo" + vista.txtNombre.getText());
            this.limpiar();
            this.deshabilitar();
            this.ActualizarTabla(db.lista());

        }
        //if que compruebe si existe o no el productosi 
        } catch (Exception e) {
            JOptionPane.showMessageDialog(vista, "Surgio un error al insertar Producto" + e.getMessage());
        }
        
        } else JOptionPane.showMessageDialog(vista, "Falta informacion");
        }
        
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   
}