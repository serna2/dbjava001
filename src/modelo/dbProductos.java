/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor..
 */
package modelo;
import java.util.ArrayList;
/**
 *
 * @author danie
 */
public class dbProductos extends dbManejados implements Persistencia{ 
    public dbProductos(){
    super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Productos pro = new Productos();
        pro =(Productos) object;
        
        String consulta = "";
        consulta = "Insert into productos(codigo, nombre, precio, fecha, status)" + " values(?,?,?,?,?)";
        
        if (this.conectar() ) {
            this.sqlConsults = this.conexion.prepareStatement(consulta);
            this.sqlConsults.setString(1, pro.getCodigo());
            this.sqlConsults.setString(2, pro.getNombre());
            this.sqlConsults.setFloat(3, pro.getPrecio());
            this.sqlConsults.setString(4, pro.getFecha());
            this.sqlConsults.setInt(5, pro.getStatus());
            this.sqlConsults.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Object object) throws Exception {
            Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "Update productos set codigo=?, nombre=?, precio=?, fecha=?" + "where idProductos = ? and status = 0";
        if (this.conectar()) {
            this.sqlConsults = this.conexion.prepareStatement(consulta);
            this.sqlConsults.setString(1, pro.getCodigo());
            this.sqlConsults.setString(2, pro.getNombre());
            this.sqlConsults.setFloat(3, pro.getPrecio());
            this.sqlConsults.setString(4, pro.getFecha());
            this.sqlConsults.setInt(5, pro.getIdProductos());

            this.sqlConsults.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void habilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "Update productos set status = 1 where idProducto = ? and status = 0";
        if (this.conectar()) {
            this.sqlConsults.setInt(1, pro.getIdProductos());

            this.sqlConsults.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) object;
        String consulta = "";
        consulta = "Update productos set status = 1 where idProductos = ? and status = 0";
        if (this.conectar()) {
            this.sqlConsults.setInt(1, pro.getIdProductos());

            this.sqlConsults.executeUpdate();
            this.desconectar();
        }   
    }

    @Override
    public boolean siExiste(int id) throws Exception {
        boolean exito = false;
        String consulta = "";
        consulta = "select * from productos where idProductos = ? and status = 0";
        
        if (this.conectar()) {
            this.sqlConsults = this.conexion.prepareStatement (consulta);
            this.sqlConsults.setInt (1, id);
             registros = this.sqlConsults.executeQuery();
              if (registros.next())exito = true;
             this.desconectar();
        }
        return exito;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Productos>();
        String consulta = "select * from productos where status = 0 order by codigo";
        if (this.conectar()) {
           this.sqlConsults = this.conexion.prepareStatement(consulta);
           registros = this.sqlConsults.executeQuery();
           
            while(registros.next()) {
            Productos pro = new Productos();
                pro.setCodigo(registros.getString("codigo"));
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdProductos(registros.getInt("idproductos"));
                pro.setStatus(registros.getInt("status"));
                
                listaProductos.add(pro);
            }
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        String consulta = "select * from productos where codigo = ? and status = 0";
        
        if (this.conectar()) {
           this.sqlConsults = this.conexion.prepareStatement(consulta);
           this.sqlConsults.setString(1, codigo);
           
           registros = this.sqlConsults.executeQuery();
           
            if(registros.next()) {
                pro.setCodigo(codigo);
                pro.setNombre(registros.getString("nombre"));
                pro.setPrecio(registros.getInt("precio"));
                pro.setFecha(registros.getString("fecha"));
                pro.setIdProductos(registros.getInt("idproductos"));
                pro.setStatus(registros.getInt("status"));

            }
            this.desconectar();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    return pro;
    }    
    
}